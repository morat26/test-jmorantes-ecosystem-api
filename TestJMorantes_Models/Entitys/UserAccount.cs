﻿using System;
namespace TestJmorantes_Models.Entitys
{
    public class UserAccount
    {
        public Guid IdAccount { get; set; }
        public Guid IdUser { get; set; }
        public string Type { get; set; }
        public string AccountName { get; set; }
        public string Status { get; set; }
        public string Currency { get; set; }
        public decimal Balance { get; set; }
        public bool IsSelect { get; set; }
        public string Img { get; set; }
    }
}
