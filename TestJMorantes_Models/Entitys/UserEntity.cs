﻿using System;
namespace TestJmorantes_Models.Entitys
{
    public class UserEntity
    {

        public Guid IdUser { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string img { get; set; }
        public DateTime lastLogin { get; set; }
    }
}
