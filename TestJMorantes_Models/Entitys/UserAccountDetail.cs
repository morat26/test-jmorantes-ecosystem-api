﻿using System;
namespace TestJmorantes_Models.Entitys
{
    public class UserAccountDetail
    {
        public Guid IdDetail { get; set; }
        public Guid IdAccount { get; set; }
        public string FromData { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        public double Value { get; set; }
        public double Balance { get; set; }
    }
}
