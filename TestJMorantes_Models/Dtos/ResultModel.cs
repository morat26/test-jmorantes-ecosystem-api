﻿using System;
namespace TestJmorantes_Models.Dtos
{
    public class ResultModel<T>
    {
        public bool IsValid { get; set; }
        public T Data { get; set; }
        public string Messagge { get; set; }

    }
}
