﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TestJmorantes_Domain.Authorization;
using TestJmorantes_Models.Dtos;
using TestJmorantes_Models.Entitys;
using TestJmorantes_Repositories.DataMocks;

namespace TestJmorantes_Repositories.Authorization
{
    public class AuthorizationRepository : IAuthorization
    {
        private readonly IConfiguration _Config;

        public AuthorizationRepository(IConfiguration config)
        {
            _Config = config;

        }

        public async Task<UserEntity> AuthorizationUser(UserValidationDto userAuthentication)
        {
            var user = DataMockRepositorie.GetMockUsers().Where(x => x.User.Equals(userAuthentication.User) && x.Password.Equals(userAuthentication.Password)).FirstOrDefault();
            return await Task.FromResult(user);
        }


    }
}
