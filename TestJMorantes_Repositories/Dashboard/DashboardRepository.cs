﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestJmorantes_Domain.Dashboard;
using TestJmorantes_Models.Entitys;
using TestJmorantes_Repositories.DataMocks;

namespace TestJmorantes_Repositories.Dashboard
{
    public class DashboardRepository : IDashboard
    {
        public async Task<List<UserAccount>> GetUserAccount(Guid idUser)
        {
            var userAccount = DataMockRepositorie.GetMockUserAccount().Where(x => x.IdUser.Equals(idUser)).ToList();
            return await Task.FromResult(userAccount);
        }

        public async Task<List<UserAccountDetail>> GetUserAccountDetails(Guid idAccount)
        {
            var userAccountDetail = DataMockRepositorie.GetMockUserAccountDetails().Where(x => x.IdAccount.Equals(idAccount)).OrderBy(x => x.FromData).ToList();
            return await Task.FromResult(userAccountDetail);
        }
    }
}
