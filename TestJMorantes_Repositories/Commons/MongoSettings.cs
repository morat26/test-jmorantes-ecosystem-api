﻿using System;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
namespace TestJmorantes_Repositories.Commons
{
    public  class MongoSettings
    {
        private readonly IConfiguration _Config;

        public MongoSettings(IConfiguration config)
        {
            _Config = config;
        }

        public MongoClient UsingMongoDBConfig() {
            var MongoDatabaseName = _Config["Settings:MongoDB"]; 
            var MongoUsername = _Config["Settings:MongoUser"];
            var MongoPassword = _Config["Settings:MongoPassword"];
            var MongoPort = _Config["Settings:MongoPort"];
            var MongoHost = _Config["Settings:MongoServer"];

            MongoCredential credential = MongoCredential.CreateCredential(MongoDatabaseName,MongoUsername, MongoPassword);
            var settings = new MongoClientSettings
            {
                Credentials = new[] { credential },
                Server = new MongoServerAddress(MongoHost, Convert.ToInt32(MongoPort))
            };
            //string conectionString = $"mongodb://{MongoUsername}:{MongoPassword}@{MongoHost}:{MongoPort}/{MongoDatabaseName}?authSource=admin";
            MongoClient mongoClient = new MongoClient(settings);
            return mongoClient;

        }

    }
}
