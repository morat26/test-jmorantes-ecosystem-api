﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;

namespace TestJmorantes_Repositories.Commons
{
    public class SimpleMapper
    {
        public static T map<T>(object origin, T dest, bool typeLowerCase)
        {
            if (origin == null)
            {
                return default(T);
            }

            var properties = origin.GetType();

            var originProperties = dest.GetType().GetProperties();

            foreach (var prop in originProperties)
            {
                var nameProperty = prop.Name;

                if (typeLowerCase) {
                    nameProperty = prop.Name.Substring(0, 1).ToLower()+ prop.Name.Substring(1);
                }


                PropertyInfo value = properties.GetProperty(nameProperty);

                if (value != null)
                {
                    prop.SetValue(dest, value.GetValue(origin, null), null);
                }
            }
            return dest;
        }

        public static T map<T>(object origin, bool typeLowerCase)
        {
            T dest = Activator.CreateInstance<T>();
            return map<T>(origin, dest, typeLowerCase);
        }

        public static List<T> MapCollection<T>(dynamic Data, bool typeLowerCase)
        {

            List<T> results = new List<T>();
            foreach (var item in Data)
            {
                results.Add(map<T>(item, typeLowerCase));
            }
            return results;

        }

        public static bool IsPropertyExist(dynamic settings, string name)
        {
            if (settings is ExpandoObject)
                return ((IDictionary<string, object>)settings).ContainsKey(name);

            return settings.GetType().GetProperty(name) != null;
        }

    }
}
