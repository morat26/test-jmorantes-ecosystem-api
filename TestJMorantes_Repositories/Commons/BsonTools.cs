﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;

namespace TestJmorantes_Repositories.Commons
{
    public  static class BsonTools
    {
        
        public static List<T> Deserialize<T>(this List<BsonDocument> document)
        {
            List<T> data = new List<T>();
            foreach (var item in document)
            {
                var info = BsonSerializer.Deserialize<T>(item);
                data.Add(info);
            }
            return data;
        }
    }
    
}
