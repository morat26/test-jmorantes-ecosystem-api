﻿using System;
using System.Collections.Generic;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Repositories.DataMocks
{
    public static class DataMockRepositorie
    {
        public static List<UserEntity> GetMockUsers()
        {
            List<UserEntity> users = new List<UserEntity>() {
                new UserEntity{
                    IdUser = new Guid("80e2ae45-e1bd-4c7b-80dd-08e1e9669891"),
                    User = "jmorantes",
                    Password = "1234",
                    Name = "Julian Morantes",
                    img = "jmorantes.jpg",
                    lastLogin = new DateTime()
                },
                new UserEntity{
                    IdUser = new Guid("c68d36c2-5b91-411c-98cf-755d5249e1ea"),
                    User = "test1",
                    Password = "test",
                    Name = "I`m Groot",
                    img = "groot.jpeg",
                    lastLogin = new DateTime()
                }
            };
            return users;
        }

        public static List<UserAccount> GetMockUserAccount()
        {
            List<UserAccount> users = new List<UserAccount>() {
                new UserAccount{
                    IdAccount = new Guid("a6b0b5c5-c7ea-4a59-8885-77445594f687"),
                    IdUser = new Guid("80e2ae45-e1bd-4c7b-80dd-08e1e9669891"),
                    Type = "Checking",
                    AccountName= "1234555**** - GOLD",
                    Status= "Active", 
                    Currency= "USD",
                    Balance= 8500000,
                    IsSelect= false,
                    Img= "icon-card.png"
                },
                new UserAccount{
                    IdAccount = new Guid("8983313a-3a9e-43f2-8569-465da749c10f"),
                    IdUser = new Guid("80e2ae45-e1bd-4c7b-80dd-08e1e9669891"),
                    Type = "Trading",
                    AccountName= "1523333**** - Coins",
                    Status= "Active",
                    Currency= "USD",
                    Balance= 15500000,
                    IsSelect= false,
                    Img= "icon-save.png"
                },
                 new UserAccount{
                    IdAccount = new Guid("5e54b559-e69e-4285-b33a-816e952fec69"),
                    IdUser = new Guid("80e2ae45-e1bd-4c7b-80dd-08e1e9669891"),
                    Type = "Savings",
                    AccountName= "52312333**** - TEST",
                    Status= "Active",
                    Currency= "USD",
                    Balance= 8000000,
                    IsSelect= false,
                    Img= "icon-save.png"
                },
                new UserAccount{
                    IdAccount = new Guid("f88f05dd-70b9-4803-9ff6-b837d8df5ed9"),
                    IdUser = new Guid("80e2ae45-e1bd-4c7b-80dd-08e1e9669891"),
                    Type = "Trading",
                    AccountName= "51313400**** - FAIL",
                    Status= "Desactivated",
                    Currency= "USD",
                    Balance= 0,
                    IsSelect= false,
                    Img= "icon-card.png"
                },
                new UserAccount{
                    IdAccount = new Guid("2c21a113-15a8-4bfc-9a8c-8bff6fa0e542"),
                    IdUser = new Guid("c68d36c2-5b91-411c-98cf-755d5249e1ea"),
                    Type = "Savings",
                    AccountName= "548833**** - CLASIC",
                    Status= "Desactivated",
                    Currency= "USD",
                    Balance= 5500000,
                    IsSelect= false,
                    Img= "icon-card.png"
                }

            };
            return users;
        }

        public static List<UserAccountDetail> GetMockUserAccountDetails()
        {
            List<UserAccountDetail> accountDetail = new List<UserAccountDetail>() {
                new UserAccountDetail{
                    IdDetail = new Guid(), 
                    IdAccount= new Guid("a6b0b5c5-c7ea-4a59-8885-77445594f687"),
                    FromData= "15/05/2021",
                    Description= "TRANSACTION",
                    Currency= "USD",
                    Value= 1234, 
                    Balance= 1234
                },
                 new UserAccountDetail{
                    IdDetail = new Guid(), 
                    IdAccount= new Guid("a6b0b5c5-c7ea-4a59-8885-77445594f687"),
                    FromData= "18/05/2021",
                    Description= "TRANSACTION",
                    Currency= "USD",
                    Value= 34.34, 
                    Balance= 34.34
                },
                new UserAccountDetail{
                    IdDetail = new Guid(), 
                    IdAccount= new Guid("a6b0b5c5-c7ea-4a59-8885-77445594f687"),
                    FromData= "19/05/2021",
                    Description= "PAYMENT VIRT AS",
                    Currency= "USD",
                    Value= 62.34, 
                    Balance= 62.34
                },
                new UserAccountDetail{
                    IdDetail = new Guid(),
                    IdAccount= new Guid("a6b0b5c5-c7ea-4a59-8885-77445594f687"),
                    FromData= "18/05/2021",
                    Description= "TRANSACTION",
                    Currency= "USD",
                    Value= 12300,
                    Balance= 12300
                },
                new UserAccountDetail{
                    IdDetail = new Guid(),
                    IdAccount= new Guid("8983313a-3a9e-43f2-8569-465da749c10f"),
                    FromData= "15/05/2021",
                    Description= "TRANSACTION",
                    Currency= "USD",
                    Value= 12.34,
                    Balance= 12.34
                },
                new UserAccountDetail{
                    IdDetail = new Guid(),
                    IdAccount= new Guid("8983313a-3a9e-43f2-8569-465da749c10f"),
                    FromData= "15/05/2021",
                    Description= "TRANSACTION",
                    Currency= "USD",
                    Value= 4880.21,
                    Balance= 4880.21
                },
            };

            return accountDetail;
        }
    }
}
