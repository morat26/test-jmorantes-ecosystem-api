﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using TestJmorantes_Domain.Authorization;
using TestJmorantes_Models.Dtos;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Api.Controllers
{

    [Authorize]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private AuthorizationDomain _AuthenticationDomain;
        private readonly IConfiguration _Config;
        public AuthorizationController(IAuthorization autehntication, IConfiguration config)
        {
            _AuthenticationDomain = new AuthorizationDomain(autehntication);
            _Config = config;
        }

        [HttpPost]
        [EnableCors("*")]
        [Route("AuthorizationUser")]
        public async Task<ActionResult<ResultModel<UserEntity>>> AuthorizationUser([FromBody] UserValidationDto data)
        {
            try
            {
                if (String.IsNullOrEmpty(data.User) || String.IsNullOrEmpty(data.Password))
                {
                    return BadRequest();
                }

                var result = await _AuthenticationDomain.AuthorizationUser(data);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

    }
}
