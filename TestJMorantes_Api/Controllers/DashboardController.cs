﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using TestJmorantes_Domain.Dashboard;
using TestJmorantes_Models.Dtos;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Api.Controllers
{
    [Authorize]
    [ApiController]
    public class DashboardController : ControllerBase
    {

        private readonly IConfiguration _Config;

        
        private DashBoardDomain _DashBoardDomain;

        public DashboardController(IDashboard dashboard, IConfiguration config)
        {
            _Config = config;
            _DashBoardDomain = new DashBoardDomain(config, dashboard);
        }

        [HttpGet]
        [Route("UserAccount")]
        public async Task<ActionResult<ResultModel<List<UserAccount>>>> GetUserAccount(Guid idUser)
        {
            try
            {
                if (idUser == null)
                {
                    return BadRequest();
                }

                var result = await _DashBoardDomain.GetUserAccount(idUser);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("UserAccountDetails")]
        public async Task<ActionResult<ResultModel<List<UserAccount>>>> GetUserAccountDetails(Guid idAccount)
        {
            try
            {
                if (idAccount == null)
                {
                    return BadRequest();
                }

                var result = await _DashBoardDomain.GetUserAccountDetails(idAccount);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }

        [HttpPost]
        [Route("Product")]
        public async Task<ActionResult<ResultModel<bool>>> AddNewProduct([FromBody] UserAccount acount)
        {
            try
            {
                if (acount == null)
                {
                    return BadRequest();
                }

                var result = await _DashBoardDomain.AddNewProduct(acount);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
