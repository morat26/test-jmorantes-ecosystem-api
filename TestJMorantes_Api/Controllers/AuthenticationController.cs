﻿
using TestJmorantes_Domain.Authentication;
using TestJmorantes_Models.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;

namespace TestJmorantes_Api.Controllers
{

    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private AuthenticationDomain _AuthenticationDomain;
        private readonly IConfiguration _Config;
        public AuthenticationController( IConfiguration config)
        {
            _AuthenticationDomain = new AuthenticationDomain(config);
            _Config = config;
        }

        [HttpPost]
        [Route("Authentication")]
        public async Task<ActionResult<String>> Authentication(TokenAppDto data) {
            try
            {
                if (String.IsNullOrEmpty(data.AppId)) {
                    return BadRequest(_Config["Settings:ErrorNotData"]); 
                }

                var result = _AuthenticationDomain.AuthenticationApi(data.AppId);
                if (result.IsValid == true) {
                    return Ok(result);
                }

                return BadRequest(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            
        }
    }
}
