﻿
using Microsoft.Extensions.DependencyInjection;
using TestJmorantes_Domain.Authorization;
using TestJmorantes_Domain.Dashboard;
using TestJmorantes_Repositories.Authorization;
using TestJmorantes_Repositories.Dashboard;

namespace HomeLife_Api.DependencyManager
{
    public class DependecyManager
    {
        public void ConfigurateServices(ref IServiceCollection service) {
            service.AddRazorPages();
            service.AddScoped<IAuthorization, AuthorizationRepository>();
            service.AddScoped<IDashboard, DashboardRepository>();
        }
    }
}
