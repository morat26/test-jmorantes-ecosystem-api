﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Domain.Dashboard
{
    public interface IDashboard
    {
        Task<List<UserAccount>> GetUserAccount(Guid idUser);
        Task<List<UserAccountDetail>> GetUserAccountDetails(Guid idAccount);
    }
}
