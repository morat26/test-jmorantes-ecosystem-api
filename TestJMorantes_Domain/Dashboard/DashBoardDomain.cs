﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TestJmorantes_Models.Dtos;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Domain.Dashboard
{
    public class DashBoardDomain
    {
        private readonly IConfiguration _Config;
        private readonly IDashboard _Dashboard;

        public DashBoardDomain(IConfiguration config, IDashboard dashboard)
        {
            _Config = config;
            _Dashboard = dashboard;
        }

        public async Task<ResultModel<List<UserAccount>>> GetUserAccount(Guid idUser)
        {
            
            ResultModel<List<UserAccount>> result = new ResultModel<List<UserAccount>> { IsValid = true };
            try
            {
                result.Data = await _Dashboard.GetUserAccount(idUser);
                result.Messagge = "Ususario con permisos";
            }
            catch (Exception ex)
            {
                result.IsValid = false;
                result.Messagge = ex.Message;
            }

            return result;
   
        }

        public async Task<ResultModel<List<UserAccountDetail>>> GetUserAccountDetails(Guid idAccount)
        {
            ResultModel<List<UserAccountDetail>> result = new ResultModel<List<UserAccountDetail>> { IsValid = true };
            try
            {
                result.Data = await _Dashboard.GetUserAccountDetails(idAccount);
                result.Messagge = "Ususario con permisos";
            }
            catch (Exception ex)
            {
                result.IsValid = false;
                result.Messagge = ex.Message;
            }

            return result;
        }

        public async Task<ResultModel<bool>> AddNewProduct(UserAccount acount)
        {
            return new ResultModel<bool>();
        }
    }
}
