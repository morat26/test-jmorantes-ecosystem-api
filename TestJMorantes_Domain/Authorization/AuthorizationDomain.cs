﻿using System;
using System.Threading.Tasks;
using TestJmorantes_Models.Dtos;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Domain.Authorization
{
    public class AuthorizationDomain
    {
        private readonly IAuthorization _Authentication;

        public AuthorizationDomain(IAuthorization authentication)
        {
            _Authentication = authentication;
        }

        public async  Task<ResultModel<UserEntity>> AuthorizationUser(UserValidationDto userAuthentication)
        {
            ResultModel<UserEntity> result = new ResultModel<UserEntity>{IsValid = true };
            try
            {
                var usuario =  await _Authentication.AuthorizationUser(userAuthentication);
                if (usuario == null) {
                    result.Messagge = "Usuario No encontrado";
                    result.IsValid = false;
                    return result;
                }

                result.Data = usuario;
                result.Messagge = "Ususario con permisos";
            }
            catch (Exception ex)
            {
                result.IsValid = false;
                result.Messagge = ex.Message;
            }

            return result;
        }
    }
}
