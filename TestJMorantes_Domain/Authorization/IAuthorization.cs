﻿

using System.Threading.Tasks;
using TestJmorantes_Models.Dtos;
using TestJmorantes_Models.Entitys;

namespace TestJmorantes_Domain.Authorization
{
    public interface IAuthorization
    {
        Task<UserEntity> AuthorizationUser(UserValidationDto userAuthentication);
    }
}
