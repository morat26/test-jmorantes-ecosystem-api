﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TestJmorantes_Domain.Helpers;
using TestJmorantes_Models.Dtos;

namespace TestJmorantes_Domain.Authentication
{
    public class AuthenticationDomain
    {
        private readonly IConfiguration _Config;
        private JwtGenerator _JwtGenerator;
        public AuthenticationDomain(IConfiguration config)
        {
            _Config = config;
            _JwtGenerator = new JwtGenerator(config);
        }

        public ResultModel<string> AuthenticationApi(string keyApp)
        {
            ResultModel<string> result = new ResultModel<string> { IsValid = true };
            try
            {
                var keys = _Config["Settings:AppsId"].Split("|");
                var validKey = keys.Where(x => x.Equals(keyApp)).FirstOrDefault();
                if (String.IsNullOrEmpty(validKey))
                {
                    result.IsValid = false;
                    result.Messagge = _Config["Settings:ErrorNotAppKey"];
                    return result;
                }

                result.Data = _JwtGenerator.GenerateJwtToken();
                result.Messagge = _Config["Settings:SuccessAppKey"];
            }
            catch (Exception ex)
            {
                result.IsValid = false;
                result.Messagge = ex.Message;
            }
            return result;
        }
    }
}
